﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RestApiConsumer
{
    class Program
    {


        static async Task Main(string[] args)
        {

            PostRequest("https://api.autocrib.net/v1/item", "https://identity.autocrib.net/connect/token").GetAwaiter().GetResult();


        }

        static async Task insertAzure(
                    string abcClassp = "",
                    string accountNop = "",
                    Boolean activep = false,
                    String adjustedStandardPriceDatep = "",
                    double ? adjustedStandardPricep = null,
                    double ? binHeightp = null,
                    long ? binSizep = null,
                    string customField1p = "",
                    string customField2p = "",
                    string customField3p = "",
                    string customField4p = "",
                    string customField5p = "",
                    string customField6p = "",
                    string customField7p = "",
                    string customField8p = "",
                    string customField9p = "",
                    string customField10p = "",
                    string customField11p = "",
                    string customField12p = "",
                    DateTime ? dateCreatedp = null,
                    string description1p = "",
                    string description2p = "",
                    Boolean fillToMaxp = false,
                    Boolean fodControlp = false,
                    long fullChargep = 0,
                    double ? issueCostp = null,
                    string itemClassp = "",
                    string itemGroupp = "",
                    string itemNop = "",
                    long ? itemTypep = null,
                    Boolean kitp = false,
                    DateTime ? lastIssuep = null,
                    DateTime ? lastReviewp = null,
                    DateTime ?lastUpdatedp = null,
                    double ? lastYearCostp = null,
                    long ? lastYearIssuesp = null,
                    long ? leadTimep = null,
                    DateTime ? leadTimeLockDatep = null,
                    Boolean lotControlp = false,
                    Boolean lotExpirationp = false,
                    string manufacturerp = "",
                    string manufacturerItemp = "",
                    long ? maxSystemQuantityp = null,
                    long ? minimumChargep = null,
                    double ? monthToDateCostp =null,
                    long ? monthToDateIssuesp = null,
                    double ? movingAvgCostp = null,
                    double ? movingAvgPricep = null,
                    string notesp = "",
                    long ? orderQuantityp = null,
                    Boolean pedp = false,
                    Boolean physicalKitp = false,
                    string purchaseClassp = "",
                    long ? recordIDp = null,
                    Boolean referenceDifferentPackageQuantityp = false,
                    string referenceItemp = "",
                    Boolean serializep = false,
                    string standardPriceDatep = "",
                    double ? standardPricep = null,
                    string statusp = "",
                    string supplierNop = "",
                    string supplierItemp = "",
                    string txBinSizep = "",
                    long ? txHeightp = null,
                    double ? unitCostp = null,
                    double ? unitPricep = null,
                    string unitOfMeasurep = "",
                    Boolean weighp = false,
                    long ? weightp = null,
                    double ? yearToDateCostp = null,
                    long ? yearToDateIssuesp = null
            )
        {
            string connString = "Server=tcp:arcturusdatabase.database.windows.net,1433;Initial Catalog=azuredatabase;Persist Security Info=False;User ID=serveradmin;Password=passwordtest?1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            using (var conn = new SqlConnection(connString))
            {
                using (var command = conn.CreateCommand())
                {
                    command.CommandText = @"
                    INSERT [dbo].[Stay] (
                        abcClass,
                        accountNo,
                        active,
                        adjustedStandardPriceDate,
                        adjustedStandardPrice,
                        binHeight,
                        binSize,
                        customField1,
                        customField2,
                        customField3,
                        customField4,
                        customField5,
                        customField6,
                        customField7,
                        customField8,
                        customField9,
                        customField10,
                        customField11,
                        customField12,
                        dateCreated,
                        description1,
                        description2,
                        fillToMax,
                        fodControl,
                        fullCharge,
                        issueCost,
                        itemClass,
                        itemGroup,
                        itemNo,
                        itemType,
                        kit,
                        lastIssue,
                        lastReview,
                        lastUpdated,
                        lastYearCost,
                        lastYearIssues,
                        leadTime,
                        leadTimeLockDate,
                        lotControl,
                        lotExpiration,
                        manufacturer,
                        manufacturerItem,
                        maxSystemQuantity,
                        minimumCharge,
                        monthToDateCost,
                        monthToDateIssues,
                        movingAvgCost,
                        movingAvgPrice,
                        notes,
                        orderQuantity,
                        ped,
                        physicalKit,
                        purchaseClass,
                        recordID,
                        referenceDifferentPackageQuantity,
                        referenceItem,
                        serialize,
                        standardPriceDate,
                        standardPrice,
                        status,
                        supplierNo,
                        supplierItem,
                        txBinSize,
                        txHeight,
                        unitCost,
                        unitPrice,
                        unitOfMeasure,
                        weigh,
                        weight,
                        yearToDateCost,
                        yearToDateIssues

                    )
                    OUTPUT INSERTED.Id
                    VALUES (
                        
                        @abcClass,
                        @accountNo,
                        @active,
                        @adjustedStandardPriceDate,
                        @adjustedStandardPrice,
                        @binHeight,
                        @binSize,
                        @customField1,
                        @customField2,
                        @customField3,
                        @customField4,
                        @customField5,
                        @customField6,
                        @customField7,
                        @customField8,
                        @customField9,
                        @customField10,
                        @customField11,
                        @customField12,
                        @dateCreated,
                        @description1,
                        @description2,
                        @fillToMax,
                        @fodControl,
                        @fullCharge,
                        @issueCost,
                        @itemClass,
                        @itemGroup,
                        @itemNo,
                        @itemType,
                        @kit,
                        @lastIssue,
                        @lastReview,
                        @lastUpdated,
                        @lastYearCost,
                        @lastYearIssues,
                        @leadTime,
                        @leadTimeLockDate,
                        @lotControl,
                        @lotExpiration,
                        @manufacturer,
                        @manufacturerItem,
                        @maxSystemQuantity,
                        @minimumCharge,
                        @monthToDateCost,
                        @monthToDateIssues,
                        @movingAvgCost,
                        @movingAvgPrice,
                        @notes,
                        @orderQuantity,
                        @ped,
                        @physicalKit,
                        @purchaseClass,
                        @recordID,
                        @referenceDifferentPackageQuantity,
                        @referenceItem,
                        @serialize,
                        @standardPriceDate,
                        @standardPrice,
                        @status,
                        @supplierNo,
                        @supplierItem,
                        @txBinSize,
                        @txHeight,
                        @unitCost,
                        @unitPrice,
                        @unitOfMeasure,
                        @weigh,
                        @weight,
                        @yearToDateCost,
                        @yearToDateIssues
                    
                    )
                        ";

                    command.Parameters.AddWithValue("@abcClass", abcClassp);
                    command.Parameters.AddWithValue("@accountNo", accountNop);
                    command.Parameters.AddWithValue("@active", activep);
                    command.Parameters.AddWithValue("@adjustedStandardPriceDate", adjustedStandardPriceDatep);
                    command.Parameters.AddWithValue("@adjustedStandardPrice", adjustedStandardPricep);
                    command.Parameters.AddWithValue("@binHeight", binHeightp);
                    command.Parameters.AddWithValue("@binSize", binSizep);
                    command.Parameters.AddWithValue("@customField1", customField1p);
                    command.Parameters.AddWithValue("@customField2", customField2p);
                    command.Parameters.AddWithValue("@customField3", customField3p);
                    command.Parameters.AddWithValue("@customField4", customField4p);
                    command.Parameters.AddWithValue("@customField5", customField5p);
                    command.Parameters.AddWithValue("@customField6", customField6p);
                    command.Parameters.AddWithValue("@customField7", customField7p);
                    command.Parameters.AddWithValue("@customField8", customField8p);
                    command.Parameters.AddWithValue("@customField9", customField9p);
                    command.Parameters.AddWithValue("@customField10", customField10p);
                    command.Parameters.AddWithValue("@customField11", customField11p);
                    command.Parameters.AddWithValue("@customField12", customField12p);
                    command.Parameters.AddWithValue("@dateCreated", dateCreatedp);
                    command.Parameters.AddWithValue("@description1", description1p);
                    command.Parameters.AddWithValue("@description2", description2p);
                    command.Parameters.AddWithValue("@fillToMax", fillToMaxp);
                    command.Parameters.AddWithValue("@fodControl", fodControlp);
                    command.Parameters.AddWithValue("@fullCharge", fullChargep);
                    command.Parameters.AddWithValue("@issueCost", issueCostp);
                    command.Parameters.AddWithValue("@itemClass", itemClassp);
                    command.Parameters.AddWithValue("@itemGroup", itemGroupp);
                    command.Parameters.AddWithValue("@itemNo", itemNop);
                    command.Parameters.AddWithValue("@itemType", itemTypep);
                    command.Parameters.AddWithValue("@kit", kitp);
                    command.Parameters.AddWithValue("@lastIssue", lastIssuep);
                    command.Parameters.AddWithValue("@lastReview", lastReviewp);
                    command.Parameters.AddWithValue("@lastUpdated", lastUpdatedp);
                    command.Parameters.AddWithValue("@lastYearCost", lastYearCostp);
                    command.Parameters.AddWithValue("@lastYearIssues", lastYearIssuesp);
                    command.Parameters.AddWithValue("@leadTime", leadTimep);
                    command.Parameters.AddWithValue("@leadTimeLockDate", leadTimeLockDatep);
                    command.Parameters.AddWithValue("@lotControl", lotControlp);
                    command.Parameters.AddWithValue("@lotExpiration", lotExpirationp);
                    command.Parameters.AddWithValue("@manufacturer", manufacturerp);
                    command.Parameters.AddWithValue("@manufacturerItem", manufacturerItemp);
                    command.Parameters.AddWithValue("@maxSystemQuantity", maxSystemQuantityp);
                    command.Parameters.AddWithValue("@minimumCharge", minimumChargep);
                    command.Parameters.AddWithValue("@monthToDateCost", monthToDateCostp);
                    command.Parameters.AddWithValue("@monthToDateIssues", monthToDateIssuesp);
                    command.Parameters.AddWithValue("@movingAvgCost", movingAvgCostp);
                    command.Parameters.AddWithValue("@movingAvgPrice", movingAvgPricep);
                    command.Parameters.AddWithValue("@notes", notesp);
                    command.Parameters.AddWithValue("@orderQuantity", orderQuantityp);
                    command.Parameters.AddWithValue("@ped", pedp);
                    command.Parameters.AddWithValue("@physicalKit", physicalKitp);
                    command.Parameters.AddWithValue("@purchaseClass", purchaseClassp);
                    command.Parameters.AddWithValue("@recordID", recordIDp);
                    command.Parameters.AddWithValue("@referenceDifferentPackageQuantity", referenceDifferentPackageQuantityp);
                    command.Parameters.AddWithValue("@referenceItem", referenceItemp);
                    command.Parameters.AddWithValue("@serialize", serializep);
                    command.Parameters.AddWithValue("@standardPriceDate", standardPriceDatep);
                    command.Parameters.AddWithValue("@standardPrice", standardPricep);
                    command.Parameters.AddWithValue("@status", statusp);
                    command.Parameters.AddWithValue("@supplierNo", supplierNop);
                    command.Parameters.AddWithValue("@supplierItem", supplierItemp);
                    command.Parameters.AddWithValue("@txBinSize", txBinSizep);
                    command.Parameters.AddWithValue("@txHeight", txHeightp);
                    command.Parameters.AddWithValue("@unitCost", unitCostp);
                    command.Parameters.AddWithValue("@unitPrice", unitPricep);
                    command.Parameters.AddWithValue("@unitOfMeasure", unitOfMeasurep);
                    command.Parameters.AddWithValue("@weigh", weighp);
                    command.Parameters.AddWithValue("@weight", weightp);
                    command.Parameters.AddWithValue("@yearToDateCost", yearToDateCostp);
                    command.Parameters.AddWithValue("@yearToDateIssues", yearToDateIssuesp);


                    conn.Open();
                    int insertedItem = (int)command.ExecuteScalar();
                }
            }


        }


        static async Task<int> PostRequest(string geturl, string posturl)
        {
            IEnumerable<KeyValuePair<string, string>> queries = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair <string,string> ("client_id","dxp_gcm"),
                new KeyValuePair <string,string> ("client_secret","9998f640-18a8-412f-8fa8-06663a2490d2"),
                new KeyValuePair <string,string> ("scope","arcturuswebapi"),
                new KeyValuePair <string,string> ("grant_type","client_credentials")
            };

            HttpContent q = new FormUrlEncodedContent(queries);
            string json;

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.PostAsync(posturl, q))
                {
                    using (HttpContent content = response.Content)
                    {
                        string myContent = await content.ReadAsStringAsync();
                        JObject obj = JObject.Parse(myContent);
                        HttpContentHeaders headers = content.Headers;

                        json = (string)obj["access_token"];

                    }


                }
                using (HttpClient getclient = new HttpClient())

                {
                    getclient.DefaultRequestHeaders.Add("dbId", "6274");
                    getclient.DefaultRequestHeaders.Authorization =
                            new AuthenticationHeaderValue("Bearer", json);

                    using (HttpResponseMessage getresponse = await getclient.GetAsync(geturl))
                    {


                        using (HttpContent getcontent = getresponse.Content)
                        {


                            string content = await getcontent.ReadAsStringAsync();
                            

                            if (!getresponse.IsSuccessStatusCode)
                            {
                                Console.WriteLine("Error calling API");
                                Console.WriteLine(getresponse.StatusCode);
                            }
                            else
                            {
                                Console.WriteLine("Sucess calling API");
                            
                            }
                           
                          
                            dynamic dynJson = JsonConvert.DeserializeObject(content);
                            foreach (var item in dynJson)
                            {
                                    insertAzure(
                                    (item.abcClass == null ? "" : item.abcClass.ToObject<string>()),
                                    (item.accountNo == null ? "" : item.accountNo.ToObject<string>()),
                                    item.active.ToObject<bool>(),
                                    (item.adjustedStandardPriceDate == null ? "" : item.adjustedStandardPriceDate.ToObject<string>()),
                                    Double.Parse(item.adjustedStandardPrice.ToString()),
                                    Double.Parse(item.binHeight.ToString()),
                                    long.Parse(item.binSize.ToString()),
                                    (item.customField2 == null ? "" : item.customField2.ToObject<string>()),
                                    (item.customField2 == null ? "" : item.customField2.ToObject<string>()),
                                    (item.customField3 == null ? "" : item.customField3.ToObject<string>()),
                                    (item.customField4 == null ? "" : item.customField4.ToObject<string>()),
                                    (item.customField5 == null ? "" : item.customField5.ToObject<string>()),
                                    (item.customField6 == null ? "" : item.customField6.ToObject<string>()),
                                    (item.customField7 == null ? "" : item.customField7.ToObject<string>()),
                                    (item.customField8 == null ? "" : item.customField8.ToObject<string>()),
                                    (item.customField9 == null ? "" : item.customField9.ToObject<string>()),
                                    (item.customField10 == null ? "" : item.customField10.ToObject<string>()),
                                    (item.customField11 == null ? "" : item.customField11.ToObject<string>()),
                                    (item.customField12 == null ? "" : item.customField12.ToObject<string>()),
                                    item.dateCreated.ToObject<DateTime>(),
                                    item.description1.ToObject<string>(),
                                    item.description2.ToObject<string>(),
                                    item.fillToMax.ToObject<bool>(),
                                    item.fodControl.ToObject<bool>(),
                                    long.Parse(item.fullCharge.ToString()),
                                    Double.Parse(item.issueCost.ToString()),
                                    (item.itemClass == null ? "" : item.itemClass.ToObject<string>()),
                                    (item.itemGroup == null ? "" : item.itemGroup.ToObject<string>()),
                                    item.itemNo.ToObject<string>(),
                                    long.Parse(item.itemType.ToString()),
                                    item.kit.ToObject<bool>(),
                                    (item.lastIssue == null ? default(DateTime) : item.lastIssue.ToObject<DateTime>()),
                                    item.lastReview.ToObject<DateTime>(),
                                    item.lastUpdated.ToObject<DateTime>(),
                                    Double.Parse(item.lastYearCost.ToString()),
                                    long.Parse(item.lastYearIssues.ToString()),
                                    long.Parse(item.leadTime.ToString()),
                                    item.leadTimeLockDate.ToObject<DateTime>(),
                                    item.lotControl.ToObject<bool>(),
                                    item.lotExpiration.ToObject<bool>(),
                                    (item.manufacturer == null ? "" : item.manufacturer.ToObject<string>()),
                                    (item.manufacturerItem == null ? "" : item.manufacturerItem.ToObject<string>()),
                                    long.Parse(item.maxSystemQuantity.ToString()),
                                    long.Parse(item.minimumCharge.ToString()), 
                                    Double.Parse(item.monthToDateCost.ToString()),
                                    long.Parse(item.monthToDateIssues.ToString()),
                                    Double.Parse(item.movingAvgCost.ToString()),
                                    Double.Parse(item.movingAvgPrice.ToString()),
                                    (item.notes == null ? "" : item.notes.ToObject<string>()),
                                    long.Parse(item.orderQuantity.ToString()),
                                    item.ped.ToObject<bool>(),
                                    item.physicalKit.ToObject<bool>(),
                                    item.purchaseClass.ToObject<string>(),
                                    long.Parse(item.recordID.ToString()),
                                    item.referenceDifferentPackageQuantity.ToObject<bool>(),
                                    (item.referenceItem == null ? "" : item.referenceItem.ToObject<string>()),
                                    item.serialize.ToObject<bool>(),
                                    (item.standardPriceDate == null ? "" : item.standardPriceDate.ToObject<string>()),
                                    Double.Parse(item.standardPrice.ToString()),
                                    item.status.ToObject<string>(),
                                    item.supplierNo.ToObject<string>(),
                                    item.supplierItem.ToObject<string>(),
                                    (item.txBinSize == null ? "" : item.txBinSize.ToObject<string>()),
                                    long.Parse(item.txHeight.ToString()),
                                    Double.Parse(item.unitCost.ToString()),
                                    Double.Parse(item.unitPrice.ToString()),
                                    item.unitOfMeasure.ToObject<string>(),
                                    item.weigh.ToObject<bool>(),
                                    long.Parse(item.weight.ToString()),
                                    Double.Parse(item.yearToDateCost.ToString()),
                                    long.Parse(item.yearToDateIssues.ToString())
                                    );


                            }
                            


                       

                        }
                    }
                }
            }
            return 0;
        }






    }

}
