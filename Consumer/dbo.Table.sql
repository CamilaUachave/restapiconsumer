﻿
CREATE TABLE [dbo].[Stay]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    	[abcClass] NVARCHAR(MAX) NULL, 
   	[accountNo] NVARCHAR(MAX) NULL, 
    	[active] BIT NULL, 
    	[adjustedStandardPriceDate] NVARCHAR(MAX) NULL, 
    	[adjustedStandardPrice] DECIMAL NULL, 
    	[binHeight] DECIMAL NULL, 
    	[binSize] INT NULL, 
    	[customField1] NVARCHAR(MAX) NULL, 
    	[customField2] NVARCHAR(MAX) NULL,
	[customField3] NVARCHAR(MAX) NULL,
	[customField4] NVARCHAR(MAX) NULL,
	[customField5] NVARCHAR(MAX) NULL,
	[customField6] NVARCHAR(MAX) NULL,
	[customField7] NVARCHAR(MAX) NULL,
	[customField8] NVARCHAR(MAX) NULL,
	[customField9] NVARCHAR(MAX) NULL,
	[customField10] NVARCHAR(MAX) NULL,
	[customField11] NVARCHAR(MAX) NULL,
	[customField12] NVARCHAR(MAX) NULL,
	[dateCreated] DATETIME NULL,
	[description1] NVARCHAR(MAX) NULL,
	[description2] NVARCHAR(MAX) NULL,
	[fillToMax] BIT NULL,
	[fodControl] BIT NULL,
	[fullCharge] INT NULL,
	[issueCost] DECIMAL NULL,
	[itemClass] NVARCHAR(MAX) NULL,
	[itemGroup] NVARCHAR(MAX) NULL,
	[itemNo] NVARCHAR(MAX) NULL,
	[itemType] INT NULL,
	[kit] BIT NULL,
	[lastIssue]  DATETIME NULL,
	[lastReview]  DATETIME NULL,
	[lastUpdated]  DATETIME NULL,
	[lastYearCost]  DECIMAL NULL,
	[lastYearIssues] INT NULL,	
	[leadTime] INT NULL,
	[leadTimeLockDate] DATETIME NULL,
	[lotControl] BIT NULL,
	[lotExpiration] BIT NULL,
 	[manufacturer] NVARCHAR(MAX) NULL,
	[manufacturerItem] NVARCHAR(MAX) NULL,
	[maxSystemQuantity] INT NULL,
	[minimumCharge] INT NULL,
	[monthToDateCost] DECIMAL NULL,
	[monthToDateIssues] INT NULL,
	[movingAvgCost] DECIMAL NULL,
	[movingAvgPrice] DECIMAL NULL,
	[notes] NVARCHAR(MAX) NULL,
	[orderQuantity] INT NULL,
	[ped] BIT NULL,
	[physicalKit] BIT NULL,
	[purchaseClass] NVARCHAR(MAX) NULL,
	[recordID] INT NULL,
	[referenceDifferentPackageQuantity] BIT NULL,
	[referenceItem] NVARCHAR(MAX) NULL,
	[serialize] BIT NULL,
	[standardPriceDate] NVARCHAR(MAX) NULL,
	[standardPrice] DECIMALT NULL,
	[status] NVARCHAR(MAX) NULL,
	[supplierNo] NVARCHAR(MAX) NULL,
	[supplierItem] NVARCHAR(MAX) NULL,
	[txBinSize] NVARCHAR(MAX) NULL,
	[txHeight] INT NULL,
	[unitCost] DECIMAL NULL,
	[unitPrice] DECIMAL NULL,
	[unitOfMeasure] NVARCHAR(MAX) NULL,
	[weigh] BIT NULL,
	[weight] INT NULL,
	[yearToDateCost] DECIMAL NULL,
	[yearToDateIssues] INT NULL,

	

)