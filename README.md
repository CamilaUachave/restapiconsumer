# RestApiConsumer
Rest Api Consumer

# Description
An algorithm to extract data from an external api and add the data to an azure database table.

![alt text](https://gitlab.com/CamilaUachave/restapiconsumer/-/blob/d3fdc7df6cbddaefd4e5e0f0d31c85b2a8767fde/API.png?raw=true)


# Build With 
- C#
- Azure (Database)

# Authors 
Camila Uachave





